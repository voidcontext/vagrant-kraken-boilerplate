vagrant-sails-boilerplate
=========================

A boilerplate for developing krakenjs apps with vagrant.
Forked from: https://github.com/voidcontext/vagrant-sails-boilerplate

### Usage

```
$ cd /path/to/project
$ git clone https://github.com/voidcontext/vagrant-sails-boilerplate.git .
$ rm -rf .git README.md LICENSE
$ # edit Cheffile
$ # edit Vagrantfile
$ vagrant plugin install vagrant-omnibus
$ vagrant plugin install vagrant-librarian-chef
$ # vagrant-hostsupdater
$ vagrant up
$ vagrant ssh
```
First steps on the vagrant box:

```
$ cd /vagrant
$ # create a new kraken app in the current folder
$ yo kraken
$ # move genrated files to /vagrant/ and remove `appName` folder, // TODO: not a good solution
$ # happy coding :)
```

